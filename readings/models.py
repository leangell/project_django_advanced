from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class User(AbstractUser):
    pass

class Lectura(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title_book = models.CharField(max_length=70)
    init_date = models.DateField()
    state = models.BooleanField(default=True)

    def __str__(self):
        return self.title_book
