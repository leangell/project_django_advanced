from django.urls import path
from .views import LoginView, DetailUser, LogoutView, RegisterView, create_lectura, update_lectura, delete_lectura

app_name = 'consumer'

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('detail_user/<int:pk>/', DetailUser.as_view(), name='detail_user'),
    path('create_lectura/', create_lectura, name='create_lectura'),
    path('update_lectura/<int:pk>', update_lectura, name='update_lectura'),
    path('delete_lectura/<int:pk>', delete_lectura, name='delete_lectura'),
]
