from celery import shared_task
from django.core.mail import send_mail
from final import settings


@shared_task
def send_email(user):
    send_mail(
        'AVISO',
        'Recuerda la lectura',
        settings.EMAIL_HOST_USER,
        [user.email]
    )
