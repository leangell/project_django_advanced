from django.shortcuts import render
from django.views.generic import TemplateView, CreateView, RedirectView, FormView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from .models import Lectura, User
from .forms import CreateLectura, LoginForm, SigninForm
from django.urls import reverse_lazy
# Create your views here.

class LoginView(FormView):
    pass

class LoginView(FormView):
    form_class = LoginForm
    template_name = 'readings/login.html'
    success_url = reverse_lazy('readings:detail_user')

    def form_valid(self, form):
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                login(self.request, user)
                return super(LoginView, self).form_valid(form)
            else:
                return self.form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('readings:detail_user', kwargs={'pk': self.request.user.pk})

class DetailUser(LoginRequiredMixin, DetailView):
    model = User
    template_name = 'readings/detail_user.html'

    def get_context_data(self, **kwargs):
        context = super(DetailUser, self).get_context_data(**kwargs)
        user = User.objects.get(pk=self.request.user.pk)
        lecturas = Lectura.objects.filter(user=user)
        context['user'] = user
        context['lecturas'] = lecturas
        return context

class LogoutView(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('consumer:login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)



class RegisterView(CreateView):
    form_class = SigninForm
    model = User
    template_name = 'readings/register.html'
    success_url = reverse_lazy('readings:login')

    def form_valid(self, form):
        form.save()
        return super(RegisterView, self).form_valid(form)


@login_required
def create_lectura(request):
    if request.method == 'POST':
        form = CreateLectura(request.POST)
        if form.is_valid():
            lectura = form.save(commit=False)
            lectura.user = request.user
            lectura.save()
            return redirect(reverse_lazy('lectura:detail_user', kwargs={'pk': request.user.pk}))
    else:
        form = CreateLectura()
    return render(request, 'lectura/create_lectura.html', {'form': form})


@login_required
def update_lectura(request, pk):
    lectura = Lectura.objects.get(pk=pk)
    if request.method == 'POST':
        form = CreateLectura(request.POST, instance=lectura)
        if form.is_valid():
            lectura = form.save(commit=False)
            lectura.user = request.user
            lectura.save()
            return redirect(reverse_lazy('lectura:detail_user', kwargs={'pk': request.user.pk}))
    else:
        form = CreateLectura(instance=lectura)
    return render(request, 'consumer/create_lectura.html', {'form': form})

@login_required
def delete_lectura(request, pk):
    lectura = Lectura.objects.get(pk=pk)
    lectura= lectura.delete()
    return redirect(reverse_lazy('lectura:detail_user', kwargs={'pk': request.user.pk}))
