from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm


from .models import Lectura, User
class LoginForm(AuthenticationForm):
    pass

class SigninForm(UserCreationForm):
    email = forms.EmailField(label='email')
    first_name = forms.CharField(label='first name')
    last_name = forms.CharField(label='last name')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')



class CreateLectura(forms.ModelForm):
    title_book = forms.CharField(label='Title',
                            max_length=70,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control',
                                       'placeholder': 'Enter your title'}))
    state = forms.BooleanField(label='Complete',
                                    required=True,
                                    widget=forms.CheckboxInput(
                                        attrs={'class': 'form-check-input',
                                               'type': 'checkbox',
                                               'role': 'switch',
                                               'id': 'flexSwitchCheckDefault'}))
    init_date= forms.DateField(label='Init Date',
                               widget=forms.DateInput(
                                   attrs={'class': 'form-control input-element',
                                          'placeholder': 'YYYY-MM-DD'}))

    class Meta:
        model = Lectura
        fields = ('title_book', 'state', 'date')
